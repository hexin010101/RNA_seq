# 一、RNA比对和转录本定量


双端测序，测序公司每个样本会给你两个文件，分别命名为sampleName.R1.fq.gz和sampleName.R2.fq.gz
## 1、RNA seq测序数据质控

```angular2html
#安装软件
conda install -c bioconda fastqc
conda install -c bioconda fastp


fastqc sampleName.R1.fq.gz -d . -o .
#然后检查fastqc的输出

fastp \
-i sampleName.R1.fq.gz \
-I sampleName.R2.fq.gz \ 
-o sampleName_R1.trimmed.fq.gz \
-O sampleName_R2.trimmed.fq.gz \
-j sampleName.fastp.json \
-h sampleName.html

```

## 2、 参考基因组建立索引 (仅需做一次即可，以后都可以拿来调用)
此处的比对代码和GTEx项目保持一致(https://github.com/broadinstitute/gtex-pipeline/tree/master/rnaseq)
```angular2html

conda install -c bioconda star
conda install -c bioconda samtools 

STAR \
  --runMode genomeGenerate \
  --genomeDir ./ \
  --genomeFastaFiles   hg38.fa  \
  --sjdbGTFfile hg38.ncbiRefSeq.gtf  \
  --sjdbOverhang 75 \
  --runThreadN 12



```

## 3、基因组比对 

```angular2html

STAR \
--readFilesIn ../raw/test_R1.fq.gz ../raw/test_R2.fq.gz \
--outFileNamePrefix test/test. \
--outSAMattrRGline ID:rg1 SM:sm1 \
--runThreadN 4  \
--genomeDir ../ref/ \
--twopassMode Basic  \
--outFilterMultimapNmax 20 \
--alignSJoverhangMin 8 \
--runMode alignReads \
--alignSJDBoverhangMin 1 \
--outFilterMismatchNmax 999 \
--outFilterMismatchNoverLmax 0.1 \
--alignIntronMin 20 --alignIntronMax 1000000 --alignMatesGapMax 1000000 \
--outFilterScoreMinOverLread 0.33 \
--outFilterMatchNmin 0 --outFilterMatchNminOverLread 0.33  \
--limitSjdbInsertNsj 1200000  --readFilesCommand zcat  \
--outSAMstrandField intronMotif \
--outFilterIntronMotifs None  \
--alignSoftClipAtReferenceEnds Yes \
--quantMode TranscriptomeSAM GeneCounts \
--outSAMtype BAM SortedByCoordinate \
--outSAMunmapped Within \
--genomeLoad NoSharedMemory



```

## 4、转录本定量

```

 
 
# 计算reads counts用于基因差异表达
conda  install -c bioconda subread

featureCounts -T 4 -s 2 -a ../../ref/hg38.refGene.gtf -o feature_counts.summary.txt *bam
grep -v "#" feature_counts.summary.txt |cut -f 1,7- > readcount.matrix.txt
grep -v "#" featurecount.summary.txt |cut -f 1,6  > Gene.length.txt

计算TPM值可以用TBtools

```
---

# 二、差异表达分析
准备文件为两个，一个是normal.csv 另一个是tumor.csv。将这两个分组做差异表达分析。
```angular2html


library(DESeq2)


# DEseq2要求输入数据是由整数组成的矩阵（即原始COUNT矩阵）


setwd('C:/Users/hexin/Desktop/RNA_seq/')
##1.读入我们前面整理出的肿瘤和正常样本COUNT矩阵文件
normal <- read.csv("normal_matrix.csv",header = T, row.names = 1)
tumor <- read.csv("tumor_matrix.csv",header = T, row.names = 1)
#这里因为TCGA中结直肠癌的tumor样本有476个，而normal样只有41个。
#样本数差异过大可能对差异表达分析结果有影响，所以这里我只取40个tumor样本。

View(normal)

tumor <- tumor[1:40]
#合并
data <- merge(tumor,normal, by = "row.names", all = TRUE)
rownames(data) <- data$Row.names
data <- data[-1]

library(tidyverse)
data %>% 
  as_tibble()

##数据预处理
#去除0值：移除那些在大于20%的样本中表达量为0的基因。
zero_counts <- apply(data == 0, 1, sum)
data <- data[-which(zero_counts>ncol(data)*0.2),]

#data <- data[-which(zero_counts>0),]  #仅保留那些在所有样本中都表达的基因

##2.构建分组矩阵
group_list <- c(rep('tumor',ncol(tumor)),rep('normal',ncol(normal)))
condition <- factor(group_list)
coldata <- data.frame(row.names = colnames(data), condition)


## 3.制作dds对象，构建差异基因分析所需的数据格式
dds <- DESeqDataSetFromMatrix(countData = data, colData = coldata, design = ~ condition)

## 4.进行差异分析
dds <- DESeq(dds)	

## 5.提取结果
result <- as.data.frame(results(dds)) # results()从DESeq分析中提取出结果表

## 6.#提取显著差异表达基因的矩阵
DGE <-subset(result,padj < 0.01 & (log2FoldChange > 5 | log2FoldChange < -5))
DGE <- DGE[order(DGE$log2FoldChange),]

#DGE_matrix <- data[rownames(DGE),]

head(DGE)

## 7.保存到文件
write.csv(DGE,'DGE.csv',row.names = TRUE)



# BiocManager::install("EnhancedVolcano")
library(EnhancedVolcano)


EnhancedVolcano(result,
                lab = rownames(result),
                x = "log2FoldChange",
                y = "padj",
                pCutoff = 10e-12,
                FCcutoff = 2
                
                )
```
---
# 三、GO和KEGG富集分析
对于人类等19种模式生物，AnnotationHub提供了基因的功能注释。
然而对于非模式生物，我们需要手动提取甚至自己注释GO和KEGG编号。

## 1、首先提取蛋白序列提交到eggNogMapper在线网站，注释完成后会给你发邮件提醒，下载 out.emapper.annotations 文件。

## 2、解析eggNOG的结果文件

```angular2html
python parse_eggNOG.py -i out.emapper.annotations \
                       -g go.tb \
                       -O hsa,mcc \
                       -o output


「-i」 eggNOG的注释结果
「-g」 上一步根据obo解析出来的文件
「-O」 参考物种(只用于KEGG注释，使用KEGG三字母物种缩写表示).
设置这个参数的原因是我做KEGG富集的时候发现有的基因会出现在非常荒唐的通路上，
比如某个植物基因富集到了癌症的相关通路，后来发现原因是有的比较基础的KO可能与癌症通路有关，
如果不使用参考物种，直接用KO去寻找map的话就会出现上述的情况。
这里使用参考物种可以把没有出现在参考物种中的通路给过滤掉。
植物可以选择拟南芥和水稻作为参考，同样的如果做动物的话，可以考虑设置一些动物物种来排除富集到植物的通路上，
具体的缩写可以参考https://rest.kegg.jp/list/organism,这里选择了人类和猴子作为参考

「-o」 输出结果文件夹。会在该文件夹生成GOannotation.tsv和KOannotation.tsv两个文件

```
## 三、富集分析

```angular2html
library(tidyverse)
library(clusterProfiler)

KOannotation <- read.delim("GO/KOannotation.tsv",stringsAsFactors=FALSE)
GOannotation <- read.delim("GO/GOannotation.tsv",stringsAsFactors=FALSE)
GOinfo <- read.delim("GO/go.tb",stringsAsFactors=FALSE)


gene<- read.csv("DGE.csv")

GOannotation = split(GOannotation, with(GOannotation, level))

res <- enricher(gene,
    TERM2GENE=GOannotation[['MF']][c(2,1)], # MF BP CC
    TERM2NAME=GOinfo[1:2]
    )

kegg <- enricher(gene,
    TERM2GENE=KOannotation[c(3,1)],
    TERM2NAME=KOannotation[c(3,4)]
    )

mutate(res, qscore = -log(p.adjust, base=20)) %>% 
  barplot(x="qscore")

dotplot(res,showCategory = 10) +
  theme_classic()


dotplot(kegg)
barplot(kegg)


```
